import React , {useEffect , useState} from 'react';
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css'
import Card from 'react-bootstrap/Card'

function Deaths() {
    
    const [latest ,setLatest] = useState([])

    useEffect(()=>{
        axios.get("https://corona.lmao.ninja/v2/all")
          .then(res =>{
            setLatest(res.data);
          })
          .catch(err => {
            console.log(err)
          })
      }, []);
    
      const date = new Date(parseInt(latest.updated))
      const lastUpdated = date.toString();
    
  return (
    <div>
        <Card bg="danger" text={"white"} className="text-center" style={{margin:"10px"}}  >
            <Card.Body>
                <Card.Title>جان باختگان</Card.Title>
                <Card.Text>{latest.deaths}</Card.Text>
            </Card.Body>
            <Card.Footer>
                <small >Last updated {lastUpdated}</small>
            </Card.Footer>
        </Card>
    </div>
  );
}

export default Deaths;
