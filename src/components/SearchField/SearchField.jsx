import React , {useEffect , useState} from 'react';
import Form from 'react-bootstrap/Form'


function SearchField(props) {
    
    const [ searchCountry , setSearchCountry ] = useState("")



    return (
        <div 
        style={{
            textAlign:"right" ,
            direction: "rtl", 
            margin:"10px" }}>
        <Form>
            <Form.Group controlId="formGroupSearch">
                 <Form.Control 
                 type="text" 
                 placeholder="جستجو" 
                 onChange={e => setSearchCountry(e.target.value)}
                  />
            </Form.Group>
        </Form>
        </div>
    );
}

export default SearchField;